# 2020-1 아주대학교 경영빅데이터센터 겨울방학 스터디그룹 - 분산머신러닝처리 #

2020년 1학기 분산머신러닝처리를 공부하는 스터디그룹 git repository 입니다.

### 본 스터디그룹에서는 무엇을 공부하나요? ###

* 파이썬의 OOP를 활용한 커스텀 Scikit-Learn Pipeline Tunning
* 요새 유행하는 머신 러닝 알고리즘 관련 학습
* 대용량 데이터를 처리할때 주의해야 할 이슈
* 클라우드(AWS, GCP, Azure..)를 활용하는 분산화 학습법

### 스터디 시간 ###
* 화요일 오후 6시 | 토요일 오후 1시 (매 세션마다 스터디 소요 시간은 유동적입니다)

### 일정 ###

* 1주차 : Course Introduction, Pipeline 처리를 통한 머신 러닝 예제 리뷰
* 2주차 : Unsupervised Machine Learning(비지도학습)
* 3주차 : Supervised Machine Learning(지도학습)
* 4주차 : Scikit-Learn Pipeline (1) 
* 5주차 : Scikit-Learn Pipeline (2)
* 6주차 : 대용량 데이터 처리의 개요 및 Issue
* 7주차 : 클라우드 서비스를 활용한 데이터 처리 해결

### How to Fork in Bitbucket? ###

1. 왼쪽의 + 버튼을 클릭하합니다
2. Fork this repository를 클릭합니다
3. 완료!

### Who do I talk to? ###

* 신민철, 아주대학교 정보시스템팀 <smc@ajou.ac.kr>